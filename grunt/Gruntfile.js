/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration below.
    grunticon: {
      icons: {
        options: {
          svgo: true,
          src: "../source/svgs/",
          dest: "../source/icons/",
          pngfolder: "../css/png/",
          datasvgcss: "icons.data.svg.scss",
          datapngcss: "icons.data.png.scss",
          urlpngcss: "icons.fallback.scss"
        }
      }
    },
    sass: {
      build: {
        options: {
          sourcemap: "none"
        },
        files: {
          '../public/styles.css': ['../source/css/site.scss'],
        }
      }
    },
    coffee: {
      compile: {
        files: {
          '../source/js/app.js': '../source/js/app.js.coffee'
        }
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['../source/js/vendor/*.js','../source/js/*.js'],
        dest: '../public/compiled.js'
      }
    },
    watch: {
      css: {
        options: {
          interrupt: true,
          livereload: true
        },
        files: ['../source/css/**/*.scss'],
        tasks: ['sass']
      },
      js: {
        options: {
          interrupt: true
        },
        files: ['../source/js/*.coffee'],
        tasks: ['coffee','concat','uglify']
      }
    },
    uglify: {
      build: {
        files: {
          '../public/compiled.min.js' : '../public/compiled.js'
        }
      }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: '../source/images/',
          src: ['*.{png,jpg,gif}'],
          dest: '../public/images/'
        }]
      }
    },
    compress: {
      main: {
        options: {
          mode: 'gzip'
        },
        files: [
          {src: ['../public/compiled.min.js'], dest: '../public/compiled.min.js.gz'},
          {src: ['../public/styles.css'], dest: '../public/styles.css.gz'}
        ]
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-compress');

  grunt.registerTask('default', ['copy:dev','sass','coffee','concat','uglify','imagemin','compress']);
};
