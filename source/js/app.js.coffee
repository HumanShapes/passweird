$(".btn").lettering();

$('#js-generate').click ->
	$('#hallucination').addClass 'is-visible'
	$('.js-placeholder').slideUp();
	$.ajax '/api/v1/new/',
		type: 'GET'
		dataType: 'html'
		error: (jqXHR, textStatus, errorThrown) ->
			console.log textStatus
		success: (data, textStatus, jqXHR) ->
			$('.js-placeholder').html '<div id="passweird">'+data+'</div>'
			$('.asterisk').remove();
		complete: (data, textStatus, jqXHR) ->
			setTimeout (->
				$('#hallucination').removeClass 'is-visible'
				$('.js-placeholder').slideDown ->
					$("#passweird").addClass('is-visible').fitText 1.1
			), 3000
	return false;