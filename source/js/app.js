(function() {
  $(".btn").lettering();

  $('#js-generate').click(function() {
    $('#hallucination').addClass('is-visible');
    $('.js-placeholder').slideUp();
    $.ajax('/api/v1/new/', {
      type: 'GET',
      dataType: 'html',
      error: function(jqXHR, textStatus, errorThrown) {
        return console.log(textStatus);
      },
      success: function(data, textStatus, jqXHR) {
        $('.js-placeholder').html('<div id="passweird">' + data + '</div>');
        return $('.asterisk').remove();
      },
      complete: function(data, textStatus, jqXHR) {
        return setTimeout((function() {
          $('#hallucination').removeClass('is-visible');
          return $('.js-placeholder').slideDown(function() {
            return $("#passweird").addClass('is-visible').fitText(1.1);
          });
        }), 3000);
      }
    });
    return false;
  });

}).call(this);
